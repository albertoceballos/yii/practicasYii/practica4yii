<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Marcadores;

class SiteController extends Controller
{
  
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

   
    public function actionIndex()
    {
        $consulta= Marcadores::find()
                ->all();
        return $this->render('index',['datos'=>$consulta]);
    }
    
    public function actionPrivados(){
        $consulta= Marcadores::find()
                ->where(['privado'=>1])
                ->all();

        return $this->render('index',['datos'=>$consulta]);
    }
    
    public function actionAdmin(){
        $consulta= Marcadores::find()
        ->all();
        return $this->render('admin',['datos'=>$consulta]);
    }
    

}
