<?php

use app\models\Marcadores;
use yii\helpers\Html;
$this->title = 'My Yii Application';
$marcador=new Marcadores;

//var_dump($datos);
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>Práctica 4</h2>
        <p class="lead">Administrador de marcadores</p>
    </div>

    <div class="body-content">
       <?php echo \app\widgets\Menu::widget(); ?>
        <div class="row col-md-8 col-md-offset-2">
            <div class="text-right">
            <?= Html::a('Nuevo',['marcadores/create'],['class'=>'btn btn-success']); ?>
            </div>
            <br>
            <table class="table table-bordered ">
                <tr>
                    <th><?= $marcador->getAttributeLabel('id') ?></th>
                    <th><?= $marcador->getAttributeLabel('nombre') ?></th>
                    <th><?= $marcador->getAttributeLabel('descripcion corta') ?></th>
                </tr>
                <?php 
                    foreach($datos as $registro){
                ?>
                <tr>
                    <td><?= Html::a($registro->id, ["marcadores/update",'id'=>$registro->id]) ?></a></td>
                    <td><?= Html::a($registro->nombre,["marcadores/update",'id'=>$registro->id])  ?></td>
                    <td><?= Html::a($registro->descripcion_corta ,["marcadores/update",'id'=>$registro->id])  ?></a></td>
                </tr>
                <?php
                }
                ?>
            </table>
            <div class="text-right">
            <?= Html::a('Nuevo',['marcadores/create'],['class'=>'btn btn-success']); ?>
            </div>
        </div>
    </div>
</div>



