<?php

/* @var $this yii\web\View */
use app\models\Marcadores;
use yii\helpers\Html;
$this->title = 'My Yii Application';
$marcador=new Marcadores;

//var_dump($datos);
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>Práctica 4</h2>
        <p class="lead">Administrador de marcadores</p>
    </div>

    <div class="body-content">
       <?php echo \app\widgets\Menu::widget(); ?>
        <div class="row col-md-8 col-md-offset-2">
            <p>
            <table class="table table-bordered ">
                <tr>
                    <th><?= $marcador->getAttributeLabel('id') ?></th>
                    <th><?= $marcador->getAttributeLabel('nombre') ?></th>
                    <th><?= $marcador->getAttributeLabel('descripcion corta') ?></th>
                </tr>
                <?php 
                    foreach($datos as $registro){
                ?>
                <tr>
                    <td><?= $registro->id ?></td>
                    <td><a href="http://<?=$registro->enlace ?>" target="blank"><?= $registro->nombre ?></a></td>
                    <td><a href="" data-toggle="modal" data-target="#myModal<?=$registro->id ?>"><?= $registro->descripcion_corta  ?></a></td>
                </tr>
                <div id="myModal<?=$registro->id ?>" class="modal fade" role="dialog">
                 <div class="modal-dialog">
                 <!-- Modal content-->
                   <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?= $registro->nombre ?></h4>
                    </div>
                    <div class="modal-body">
                        <p><?= $registro->descripcion_larga ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a class="btn btn-primary" href="http://<?= $registro->enlace ?>" target="blank">Visitar</a>
                    </div>
                   </div>
                 </div>
                </div>
                <?php
                }
                ?>
            </table>
        </div>
    </div>
</div>

