<?php

use yii\helpers\Html;
use app\widgets\Menu;


/* @var $this yii\web\View */
/* @var $model app\models\Marcadores */

$this->title = 'Create Marcadores';
$this->params['breadcrumbs'][] = ['label' => 'Marcadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marcadores-create">

    <?= Menu::widget(); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
