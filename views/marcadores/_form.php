<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Marcadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-sm-7">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
       ]);
     ?>
  
    <div class="form-group ">
    <?= $form->field($model, 'nombre', ['options' => ['class' => 'control-label']])
             ->textInput(['maxlength' => true, 'class'=>'']) ?>
    </div>
    <div class="form-group">
    <?= $form->field($model, 'enlace',['options' => ['class' => 'control-label']])
             ->textInput(['maxlength' => true,'class'=>'']) ?>
    </div>
    <div class="form-group">
    <?= $form->field($model, 'descripcion_corta',['options' => ['class' => 'control-label']])
             ->textInput(['maxlength' => true,'class'=>'']) ?>
    </div> 
    <div class="form-group">
    <?= $form->field($model, 'descripcion_larga',['options' => ['class' => 'control-label']])
             ->textarea(['rows' => 6,'class'=>'']) ?>
    </div>
    <div class="form-group">
        <div class="col-md-4 col-md-offset-6">
    <?= $form->field($model,'privado',['options' => ['class' => 'control-label']])
             ->dropDownList(['1' =>'si','0'=>'no'],['class'=>'']) ?>   
        </div>
    </div>
     <div class="form-group">
        <div class="col-md-offset-12">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>
     </div>
  

    <?php ActiveForm::end(); ?>

</div>
