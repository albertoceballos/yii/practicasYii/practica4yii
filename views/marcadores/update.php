<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Marcadores */

$this->params['breadcrumbs'][] = ['label' => 'Marcadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="marcadores-update">

    <h1>Modificar o Eliminar</h1>

    <?= $this->render('_formUpdate', [
        'model' => $model,
    ]) ?>

</div>
