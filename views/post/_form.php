<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Marcadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marcadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'enlace')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion_corta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion_larga')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'privado')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
