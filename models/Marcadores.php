<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marcadores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $enlace
 * @property string $descripcion_corta
 * @property string $descripcion_larga
 * @property int $privado
 */
class Marcadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marcadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'enlace', 'privado'], 'required','message'=>'{attribute} es obligatorio'],
            [['descripcion_larga'], 'string'],
            [['privado'], 'integer'],
            [['nombre', 'enlace', 'descripcion_corta'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'enlace' => 'Enlace',
            'descripcion_corta' => 'Descripcion Corta',
            'descripcion_larga' => 'Descripcion Larga',
            'privado' => 'Privado',
        ];
    }
}
