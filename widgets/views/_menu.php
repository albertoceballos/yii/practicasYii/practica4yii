<?php
    use yii\helpers\Html;
   
?>
        <div class="row barra-opciones">
            <div class="btn-group btn-group-lg" role="group" aria-label="">
                <?= Html::a('Públicos', ['site/index'],['class'=>'btn btn-default'])?>
                <?= Html::a('Privados', ['site/privados'],['class'=>'btn btn-default'])?>
                <?= Html::a('Admin', ['site/admin'],['class'=>'btn btn-default'])?>
            </div>
        </div>

